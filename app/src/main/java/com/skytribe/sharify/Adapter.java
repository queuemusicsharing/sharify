package com.skytribe.sharify;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private Activity mContext;
    private ArrayList<User> mUserList;
    private LayoutInflater mLayoutInflater = null;


    public Adapter(Activity context, ArrayList<User> userList) {

        mContext = context;
        mUserList = userList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public Object getItem(int pos) {
        return mUserList.get(pos);

    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_containter, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final User User = mUserList.get(position);

        holder.userDisplayName.setText(User.getDisplayName());
        holder.userProfilePicture.setImageBitmap(User.getProfilePicture());
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView userDisplayName;
        ImageView userProfilePicture;

        public ViewHolder(View userView) {
            super(userView);
            userDisplayName = (TextView) userView.findViewById(R.id.user_display_name);
            userProfilePicture = (ImageView) userView.findViewById(R.id.user_profile_picture);
        }
    }

}


