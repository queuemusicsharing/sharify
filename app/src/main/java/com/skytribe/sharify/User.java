package com.skytribe.sharify;

import android.graphics.Bitmap;

import java.io.Serializable;

public class User implements Serializable {

    private String UserID;
    private String DisplayName;
    private ProxyBitmap ProfilePicture;

    User(String UserID_Input, String DisplayNameInput) {
        this.UserID = UserID_Input;
        this.DisplayName = DisplayNameInput;
    }
    User(String UserID_Input) {
        this.UserID = UserID_Input;
    }
    public void SetDisplayName(String DisplayNameInput) {
        this.DisplayName = DisplayNameInput;
    }
    public void SetProfilePicture(Bitmap ProfilePictureInput) {
        ProxyBitmap mProxyBitmap = new ProxyBitmap(ProfilePictureInput);

        this.ProfilePicture = mProxyBitmap;
    }

    public Bitmap getProfilePicture() {

        try {
            return ProfilePicture.getBitmap();
        } catch (Exception e) {
            return null;
        }
    }

    public String getUserID() {
        return UserID;
    }

    public String getDisplayName() {
        return DisplayName;
    }

}
