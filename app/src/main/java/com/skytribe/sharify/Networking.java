package com.skytribe.sharify;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.io.InputStream;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.UserPublic;
import retrofit.client.Response;

public class Networking {

    NetworkingListener listener;

    String oAuthToken = "No Token";

    SpotifyApi api = new SpotifyApi();

    public interface NetworkingListener {
        void addDataToUser(Object[] userData);

        void NetworkError();
    }

    public void setListener(NetworkingListener listener) {
        this.listener = listener;
    }

    public void getUserData(String userID, AuthenticationResponse response) {

        oAuthToken = response.getAccessToken();

        Object[] params = new Object[1];
        params[0] = userID;

        new pullUserData().execute(params);
    }

    private class pullUserData extends AsyncTask {
        Object[] user = new Object[2];

        // Does the networking (not pre or post)
        @Override
        protected Object doInBackground(Object[] params) {
            final SpotifyService spotify = api.getService();
            try {
                while (oAuthToken.equals("No Token")) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.NetworkError();

            }
            api.setAccessToken(oAuthToken);

            spotify.getUser(params[0].toString(), new SpotifyCallback<UserPublic>() {
                @Override
                public void failure(SpotifyError spotifyError) {
                    spotifyError.printStackTrace();
                    listener.NetworkError();
                }

                @Override
                public void success(UserPublic userPublic, Response response) {
                    user[0] = userPublic.display_name;
                    user[1] = userPublic.images.get(0).url;
                    try {
                        new pullProfilePicture().execute(user);
                    } catch (Exception e) {
                        listener.NetworkError();
                        e.printStackTrace();
                    }

                }
            });


            return null;
        }
    }

    private class pullProfilePicture extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] user) {
            try {
                InputStream in = new java.net.URL(user[1].toString()).openStream();
                user[1] = BitmapFactory.decodeStream(in);
                in.close();
            } catch (Exception e) {
                listener.NetworkError();
                e.printStackTrace();
            }
            return user;
        }

        @Override
        protected void onPostExecute(Object userData) {
            listener.addDataToUser((Object[]) userData);

        }
    }
}
