package com.skytribe.sharify;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AddUser.AddUserDialogListener {

    private boolean isFabOpen = false;
    private FloatingActionButton fabShow, fabAdd, fabRemove;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;

    public RecyclerView completeRecyclerView;
    private ArrayList<User> userList;
    private RecyclerView.Adapter listAdapter;

    private static final int REQUEST_CODE = 1993;
    private static final String REDIRECT_URI = "yourcustomprotocol://callback"; // todo
    private static final String CLIENT_ID = "2530184e2ec94240b5b29fdfcfa7175c";
    public String oAuthToken = "No Token";
    AuthenticationResponse authResponse;
    Networking networking = new Networking();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setNetworkListener();
        initViews();
        setOnClickListeners();

        listAdapter.notifyDataSetChanged();

        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
        builder.setScopes(new String[]{"playlist-read-private", "playlist-read-collaborative", "playlist-modify-public", "playlist-modify-private", "user-library-modify"});
        AuthenticationRequest request = builder.build();
        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }

    private void setNetworkListener() {
        networking.setListener(new Networking.NetworkingListener() { // todo
            @Override
            public void addDataToUser(Object[] userData) {
                // Add Display Name to most recently added user.
                userList.get(userList.size() - 1).SetDisplayName(userData[0].toString());
                // Add profile picture to most recently added user.
                userList.get(userList.size() - 1).SetProfilePicture((Bitmap) userData[1]);
                listAdapter.notifyDataSetChanged();
                saveUserList(userList);
            }

            @Override
            public void NetworkError() {
                Snackbar.make(findViewById(R.id.main_layout), R.string.network_error, Snackbar.LENGTH_LONG).show();
                userList.remove(userList.size() - 1);
            }
        });
    }

    private void setOnClickListeners() {
        fabShow.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
        fabRemove.setOnClickListener(this);
    }
    private void initViews() {
        fabShow = (FloatingActionButton) findViewById(R.id.fabShow);
        fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd);
        fabRemove = (FloatingActionButton) findViewById(R.id.fabRemove);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        assignDataToArrayList();

        completeRecyclerView = (RecyclerView) findViewById(R.id.user_list);
        listAdapter = new Adapter(this, userList);
        completeRecyclerView.setAdapter(listAdapter);
        completeRecyclerView.setItemAnimator(new DefaultItemAnimator());
        completeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    private void saveUserList(ArrayList<User> listToSave) {
        String filename = "savedUsers.srl";
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(new File(getFilesDir(), "") + File.separator + filename));
            out.writeObject(listToSave);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private ArrayList<User> loadUserList() {
        ObjectInputStream input;
        ArrayList<User> loadedUserList = null;
        String filename = "savedUsers.srl";
        try {
            input = new ObjectInputStream(new FileInputStream(new File(new File(getFilesDir(), "") + File.separator + filename)));
            loadedUserList = (ArrayList<User>) input.readObject();
            input.close();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return loadedUserList;
    }
    private void assignDataToArrayList() {
        try {
            userList = loadUserList();
        } catch (Exception e) {
            userList = new ArrayList<User>();
        }
    }
    public void showDialog() {
        DialogFragment addUser = new AddUser();
        addUser.show(getSupportFragmentManager(), "adduser");
    }
    public void onAddUserPositiveClick(String userID) {
        userList.add(new User(userID));
        getUserData(userID);
    }

    private void getUserData(String userID) {
        networking.getUserData(userID, authResponse);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        // Checking if result comes from correct activity;
        if (requestCode == REQUEST_CODE) {
            authResponse = AuthenticationClient.getResponse(resultCode, intent);

            switch (authResponse.getType()) {
                case TOKEN:
                    oAuthToken = authResponse.getAccessToken();
                    break;
                case ERROR:
                    // handle error
                    break;
                default:
                    // handle other cases
            }
        }
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fabShow:
                animateFAB();
                break;
            case R.id.fabAdd:
                showDialog();
                break;
            case R.id.fabRemove:
                userList.clear();
                listAdapter.notifyDataSetChanged();
                saveUserList(userList);
                break;
        }
    }
    public void animateFAB() {
        if (isFabOpen) {
            fabShow.startAnimation(rotate_backward);
            fabAdd.startAnimation(fab_close);
            fabRemove.startAnimation(fab_close);
            fabAdd.setClickable(false);
            fabRemove.setClickable(false);
            isFabOpen = false;
            Log.d("Jim", "close");
        } else {
            fabShow.startAnimation(rotate_forward);
            fabAdd.startAnimation(fab_open);
            fabRemove.startAnimation(fab_open);
            fabAdd.setClickable(true);
            fabRemove.setClickable(true);
            isFabOpen = true;
            Log.d("James", "open");
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
